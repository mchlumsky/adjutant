###############################
Deploying Adjutant in Devstack
###############################

This is a guide to setting up Adjutant in a running Devstack environment.

This guide assumes you are running this in a clean Ubuntu 20.04 or Fedora 34
virtual machine with sudo access.

*****************************
Deploy Devstack with adjutant
*****************************

Prepare Devstack::

    sudo useradd -s /bin/bash -d /opt/stack -m stack

    # On Fedora, you must set the mode on /opt/stack
    sudo chmod 755 /opt/stack

    echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
    sudo -u stack -i
    git clone https://opendev.org/openstack/devstack
    cd devstack

Create a `local.conf` file with the following contents::

    [[local|localrc]]
    ADMIN_PASSWORD=password
    DATABASE_PASSWORD=password
    RABBIT_PASSWORD=password
    SERVICE_PASSWORD=password

    # You can use any git repo and branch that contains Adjutant
    enable_plugin adjutant https://opendev.org/openstack/adjutant master


Run the devstack build::

    ./stack.sh

Provided your VM has enough ram to handle a devstack install this should
take a while, but go smoothly. Ideally give your VM 5gb or more of ram, any
less can cause the devstack build to fail.

**********************************
Testing Adjutant via the CLI
**********************************

Now that the service is running, and the endpoint setup, you will want
to install the client and try talking to the service::

    sudo pip install python-adjutantclient

Setup your credentials as environment variables::

    export OS_USERNAME=admin
    export OS_PASSWORD=password
    export OS_PROJECT_NAME=demo
    export OS_USER_DOMAIN_NAME=default
    export OS_PROJECT_DOMAIN_NAME=default
    export OS_AUTH_URL=http://localhost/identity
    export OS_IDENTITY_API_VERSION=3
    export OS_REGION_NAME=RegionOne


Now lets check the status of the service::

    openstack adjutant status


What you should get is::

    {
        "error_notifications": [],
        "last_completed_task": null,
        "last_created_task": null
    }

Seeing as we've done nothing to the service yet this is the expected output.

To list the users on your current project (admin users are hidden by default)::

    openstack project user list

The above action is only possible for users with the following roles:
'admin', 'project_admin', 'project_mod'

Now lets try inviting a new user::

    openstack project user invite bob@example.com project_admin

You will then get a note saying your invitation has been sent. You can list
your project users again with 'openstack project user list' to see your invite.

Now look in the logs for Adjutant with journalctl::

    journalctl -u devstack@a-api.service

Near the bottom of the logs, you will see a print out of the email that would
have been sent to bob@example.com. There should be a line that looks like this:

  http://localhost/token/6c1871a494f44abbbfaf8a92ebb9b4b2

Normally that would direct the user to a Horizon dashboard page where they can
submit their password.

Since we don't have that running, your only option is to submit it via the CLI.
This is cumbersome, but doable. From that url in your Adjutant output, grab the
values after '.../token/'. That is bob's token. You can submit that via the
CLI::

    openstack admin task token submit <token> <json_data>
    openstack admin task token submit 6c1871a494f44abbbfaf8a92ebb9b4b2 '{"password": "123456"}'


Now if you get the user list, you will see bob is now active::

    openstack project user list

And also shows up as a user if you do::

    openstack user list


And since you are an admin, you can even take a look at the tasks themselves::

    openstack admin task list

The topmost one should be your invite, and if you then do a show using that
id you can see some details about it::

    openstack admin task show <UUID>


**********************************
Setting Up Adjutant on Horizon
**********************************
Adjutant has a Horizon UI plugin, the code and setup instructions for it can
be found `here <https://opendev.org/openstack/adjutant-ui>`_.

If you do set this up, you will want to edit the default Adjutant conf to so
that the TOKEN_SUBMISSION_URL is correctly set to point at your Horizon.
