# plugin.sh - DevStack plugin.sh dispatch script adjutant

function install_adjutant {
    git_clone $ADJUTANT_REPO $ADJUTANT_DIR $ADJUTANT_BRANCH
    setup_develop $ADJUTANT_DIR
}

function install_python_adjutantclient {
    if use_library_from_git "python-adjutantclient"; then
        git_clone_by_name "python-adjutantclient"
        setup_dev_lib "python-adjutantclient"
    else
        pip_install python-adjutantclient
    fi
}

function init_adjutant {
    recreate_database_mysql adjutant
    adjutant-api migrate
}

function start_adjutant {
    run_process $ADJUTANT_API "$ADJUTANT_API_BINARY $ADJUTANT_API_ARGS"
}

function configure_adjutant {
    sudo mkdir -m 755 -p $ADJUTANT_CONF_DIR
    sudo chown $STACK_USER $ADJUTANT_CONF_DIR
    cp $ADJUTANT_DIR/devstack/etc/adjutant/adjutant.yaml $ADJUTANT_CONF
    sed -i 's/ENGINE:.*$/ENGINE: django.db.backends.mysql/' $ADJUTANT_CONF
    sed -i "s/HOST:.*$/HOST: $DATABASE_HOST/" $ADJUTANT_CONF
    sed -i "s/PASSWORD:.*$/PASSWORD: $DATABASE_PASSWORD/" $ADJUTANT_CONF
    sed -i "s/USER:.*$/USER: $DATABASE_USER/" $ADJUTANT_CONF
    sed -i "s|auth_url:.*$|auth_url: $OS_AUTH_URL|" $ADJUTANT_CONF
}

function stop_adjutant {
    if is_service_enabled a-api ; then
        stop_process a-api
    fi
}

function create_adjutant_accounts {
    local adjutant_service=$(get_or_create_service "adjutant" \
        "admin-logic" \
        "OpenStack Admin Logic")

    get_or_create_endpoint $adjutant_service \
        "$REGION_NAME" \
        "$ADJUTANT_SERVICE_PROTOCOL://$ADJUTANT_SERVICE_HOSTPORT/v1"

    get_or_create_role "project_admin"
    get_or_create_role "project_mod"

    if ! is_plugin_enabled heat; then
        get_or_create_role "heat_stack_owner"
    fi
}

# check for service enabled
if is_service_enabled a-api; then

    if [[ "$1" == "stack" && "$2" == "pre-install" ]]; then
        # Set up system services
        echo_summary "Configuring system services Adjutant"

        if is_fedora; then
          install_package python3-mysqlclient
        fi

    elif [[ "$1" == "stack" && "$2" == "install" ]]; then
        # Perform installation of service source
        echo_summary "Installing Adjutant"
        install_adjutant

        # TODO(mchlumsky): python-adjutant depends on versions of PrettyTable that conflict with upper-constraints.txt
        # which causes devstack to fail it's run.
        # install_python_adjutantclient

    elif [[ "$1" == "stack" && "$2" == "post-config" ]]; then
        # Configure after the other layer 1 and 2 services have been configured
        echo_summary "Configuring Adjutant"
        configure_adjutant

        if is_service_enabled key; then
            create_adjutant_accounts
        fi

    elif [[ "$1" == "stack" && "$2" == "extra" ]]; then
        # Initialize and start the adjutant service
        echo_summary "Initializing Adjutant"
        init_adjutant

        echo_summary "Starting Adjutant"
        start_adjutant
    fi

    if [[ "$1" == "unstack" ]]; then
        # Shut down adjutant services
        echo_summary "Stopping Adjutant"
        stop_adjutant
    fi

    if [[ "$1" == "clean" ]]; then
        # Remove state and transient data
        # Remember clean.sh first calls unstack.sh
        sudo rm -rf ${ADJUTANT_CONF_DIR}
    fi
fi