This directory contains the adjutant devstack plugin. To configure adjutant, in the [[local|localrc]] section you will
need to enable the adjutant devstack plugin and enable the adjutant service by editing the [[local|localrc]] section of
your local.conf file.

#### Enable the plugin

To enable the adjutant plugin, add a line of the form:

    enable_plugin adjutant <GITURL> [GITREF]

where

    <GITURL> is the URL of an adjutant repository
    [GITREF] is an optional git ref (branch/ref/tag).  The default is
             master.

For example

    enable_plugin adjutant https://opendev.org/openstack/adjutant master

For more information, see the "Externally Hosted Plugins" section of
https://docs.openstack.org/devstack/latest/plugins.html
